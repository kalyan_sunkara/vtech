import React, { Component } from 'react';
import { Input, Row, Col, Button, Avatar, Card, Modal, message } from 'antd';
import { withRouter } from 'react-router-dom';
import './Configuration.css';
const axios = require('axios');
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: "",
            coins: "",
            badge_name: ""
        }
        this.Register = this.Register.bind(this)
        this.Login = this.Login.bind(this)
        this.onChange = this.onChange.bind(this)
    }
    onChange(event) {
        console.log('in login setstate');
        cookies.set("userId", event.target.value)
        this.setState({ user_id: event.target.value })
    }

    Register() {
        let body = {
            user_id: this.state.user_id
        }
        if (this.state.user_id != 0) {
            axios.post('http://localhost:8081/registration', body).then(response => {
                console.log("response", response)
                console.log("length", response.data.length)
                if (response.data.length > 0) {
                    Modal.success({ content: "you have successfully Registered and you got 100 coins" })
                } else {
                    Modal.warning({ content: "User already exist" })
                }
            })
        } else {
            message.warning("please enter user_id")
        }
    }
    Login() {
        let body = {
            user_id: this.state.user_id
        }
        if (this.state.user_id != 0) {
            axios.post('http://localhost:8081/login', body).then(response => {
                console.log("response", response.data)
                console.log("response1", response.data.length)
                if (response.data.length > 0) {
                    this.props.history.push({
                        pathname: "/User",
                        state: {
                            user_id: this.state.user_id,
                            coins: response.data[0].total_coins,
                            badge_name: response.data[0].badge_name
                        } 
                    })
                } else {
                    Modal.warning({ content: "user does not exist , please register" })
                }
            })
        } else {
            message.warning("please enter user_id")
        }
    }

    render() {
        return (
            <div>

                <Row>
                    <Col xs={9} sm={9} md={9} lg={9} />
                    <Col xs={3} sm={3} md={3} lg={3}  >

                        <img src="cri.svg" className="img" ></img>
                    </Col>
                    <Col xs={15} sm={15} md={15} lg={15} />
                    <Col xs={3} sm={3} md={3} lg={3}  >


                    </Col>
                    <Col xs={3} sm={3} md={3} lg={3} >
                    </Col>
                </Row>

                <div style={{ background: 'white', padding: '150px' }}>
                    <Card title="Login" className="padding"
                        style={{ width: 400, borderColor: "#010707", marginLeft: 600 }}>

                        <Row>
                            <Col xs={3} sm={3} md={3} lg={3} />
                            <Col xs={6} sm={6} md={6} lg={6}  >
                                <h4 style={{ margintop: 50, marginLeft: 10 }} >User_id :</h4>
                            </Col>
                            <Col xs={10} sm={10} md={10} lg={10}  >
                                < Input
                                    style={{ width: 190, marginLeft: 10 }}
                                    onChange={this.onChange}
                                    placeholder="enter user_id" /></Col><br /><br /><br />

                        </Row>
                        <Row>
                            <Col xs={10} sm={10} md={10} lg={10} />
                            <Col xs={3} sm={3} md={3} lg={3}  >
                                <Button type="primary"
                                    onClick={this.Register}>
                                    Register
                                </Button> </Col>

                            <Col xs={5} sm={5} md={5} lg={5} />

                            <Col xs={3} sm={3} md={3} lg={3}  >
                                <Button type="primary"
                                    onClick={this.Login}>
                                    Login
                                </Button>
                            </Col>
                        </Row>
                    </Card>
                </div>
            </div>
        );
    }
}

// export default Registration;
export default withRouter(Registration);
