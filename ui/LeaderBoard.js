import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Modal, Row, Col, Select, Menu, Icon, DatePicker, Button, Alert, message } from 'antd';
import img from './cri.svg';
import coin from './coin.jpg'
import image from './user.png'
import './game.css'
import Navigation from './Navigation.js';
import Cookies from 'universal-cookie';
import Axios from 'axios';
const cookies = new Cookies();

class LeaderBoard extends Component {
    constructor(props) {
        super(props);
        this.programChangeHandler = this.programChangeHandler.bind(this);
    }
    componentDidMount() {
        // Axios.get('http://localhost:8081/')
    }
    programChangeHandler(e) {
        console.log("programChangeHandler", e);
        if (e == "P") {
            console.log('in all programs');
            
        }
        else if (e == 'P1') {
            console.log('in program 1');

        }
        else if (e == 'P2'){
            console.log('in program 2');

        }
        
    }
    
    render() {
        console.log('cookies', cookies.get('userId'))

        return (
            // <div>
            //     <Row>
            //         {/* <Col xs={1} sm={1} md={1} lg={1}/> */}

            //         <Col xs={2} sm={2} md={2} lg={2}>
            //             <img src={img} className="img" />
            //         </Col>
                   
            //     </Row>
            //     <br />
            //     <br />
                
            //     <hr></hr>
            //     <Row>
            //         <Col xs={3} sm={3} md={3} lg={3}>
            //             <Navigation />
            //         </Col>
            //     </Row>
            //     <Row>
            //         <Col xs={3} sm={3} md={3} lg={3}/>
                        
            //     </Row>

            // </div>

            <div>
                <br /><br />
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2} >
                        <Row>
                            <img src={img}
                                className="img" ></img>
                        </Row>
                        <br />
                    </Col>

                    <Col xs={17} sm={17} md={17} lg={17} />
                    <Col xs={3} sm={3} md={3} lg={3} >
                        <h2>User ID : {cookies.get("userId")} </h2>
                        {/* <h2>Badge Name:{this.state.badgeName}</h2> */}
                        <h2>Total Coins : {cookies.get("totalCoins")} <img src={coin} style={{ width: 40, height: 50, marginTop: "-5px" }}></img></h2>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2} >
                        <img src={image} className="img" style={{ height: "70px", width: "70px", margin: "8px" }} />
                    </Col>
                </Row>
                <Row>

                    <hr ></hr>
                    <Col xs={3} sm={3} md={3} lg={3}>
                        <Navigation />
                    </Col>

                {/* </Row>
                <Row> */}
                    <Col xs={20} sm={20} md={20} lg={20}>
                        <br />
                        <br/>
                        <Select 
                            // className="programselect"
                            style={{ width: 200 , marginLeft: 50}}
                            showSearch
                            placeholder="Programs"
                            onChange={(e)=> this.programChangeHandler(e)}
                            size="large"
                        >
                            <Select.Option value="All programs" key="1">All Programs</Select.Option>
                            <Select.Option value="Program 1" key="2">Program 1</Select.Option>
                            <Select.Option value="Program 2" key="3">Program 2</Select.Option> 

                            </Select>
                   
                    <Row>
                        <Col xs={3} sm={3} md={3} lg={3} />
                        <Col xs={7} sm={7} md={7} lg={7}>
                            <h1>
                                PROGRAM LEADERBOARD
                       </h1>
                        </Col>
                    </Row>
 </Col>

                </Row>
               
                </div>
        );
    }
}

// export default LeaderBoard;
export default withRouter(LeaderBoard);
